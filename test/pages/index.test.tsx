import React from 'react';
import { IntlProvider } from 'react-intl';

import * as locales from '../../content/locale';
import { Home } from '../../pages/index';
import { render } from '../testUtils';

describe('Home page', () => {
  test('matches snapshot', async () => {
    const { asFragment } = render(
      <IntlProvider locale={'en'} defaultLocale={'en'} messages={locales['en']}>
        <Home />
      </IntlProvider>,
      {}
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
