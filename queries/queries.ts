import { gql } from '@apollo/client';

export const SITE_QUERY = gql`
  query {
    site(id: "60541bb457f1e20015fb15d4") {
      id
      name
      header {
        name
        type
        menuItems {
          id
          name
          slug
          description
        }
      }
    }
  }
`;

export const PAGE_QUERY = gql`
  query {
    page(
      siteId: "60541bb457f1e20015fb15d4"
      pageId: "60541f2a57f1e20015fb15da"
    ) {
      id
      name
      tags
      site
      hero {
        type
        mediaUrl
        heading
        hasAction
        actoinText
        actoinSlug
        location
      }
    }
  }
`;
