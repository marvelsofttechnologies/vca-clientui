import Head from 'next/head';
import * as React from 'react';

import { Footer } from '../components/Footer/Footer';
import { News } from '../components/NewsCard/NewsCard';
import { NewsSection } from '../components/NewsSection/NewsSection';
import { NewsWidget } from '../components/NewsWidget/NewsWidget';
import { HeaderWrapper } from '../components/Wrappers/HeaderWrapper';
import { HeroWrapper } from '../components/Wrappers/HeroWrapper';
import jsonData from '../data.json';

export const Home = (): JSX.Element => {
  const ParsedData = JSON.parse(JSON.stringify(jsonData));

  const allNews: News[] = ParsedData.news;

  return (
    <div>
      <>
        <Head>
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
          />
        </Head>
        <HeaderWrapper />
        <HeroWrapper />
        <NewsWidget news={allNews} />
        <NewsSection news={allNews} />
        <Footer />
      </>
    </div>
  );
};
export default Home;
