import Head from 'next/head';
import React from 'react';

import { Footer } from '../components/Footer/Footer';
import { MonthlyViewSection } from '../components/MonthlyViewSetion/MonthlyViewSection';
import { NewsHero } from '../components/NewsHero/NewsHero';
import { HeaderWrapper } from '../components/Wrappers/HeaderWrapper';

export const News = () => {
  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
        />
      </Head>
      <HeaderWrapper />
      <NewsHero />
      <MonthlyViewSection />
      <MonthlyViewSection />
      <MonthlyViewSection />
      <Footer />
    </>
  );
};

export default News;
