import { useQuery } from '@apollo/client';
import Head from 'next/head';
import React from 'react';

import { Footer } from '../components/Footer/Footer';
import { Hero2, HeroModel } from '../components/Hero/Hero2';
import { News } from '../components/NewsCard/NewsCard';
import { NewsSection } from '../components/NewsSection/NewsSection';
import { NewsWidget } from '../components/NewsWidget/NewsWidget';
import { HeaderWrapper } from '../components/Wrappers/HeaderWrapper';
import jsonData from '../data.json';
import { PAGE_QUERY } from '../queries/queries';

export default function Index2() {
  let page = {
    hero: {
      mediaUrl: '',
      mainText: '',
      subText: '',
      heading: '',
    },
  };
  const { loading: pageLoading, error: pageError, data: pageData } = useQuery(
    PAGE_QUERY
  );
  if (!pageLoading && !pageError) {
    page = pageData.page;
  }
  const hero: HeroModel = page.hero;
  const ParsedData = JSON.parse(JSON.stringify(jsonData));

  const allNews: News[] = ParsedData.news;
  return (
    <>
      <Head>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
        />
      </Head>
      <HeaderWrapper />
      <Hero2 hero={hero} />
      <NewsWidget news={allNews} />
      <NewsSection news={allNews} />
      <Footer />
    </>
  );
}
