import React from 'react';

export function Footer() {
  return (
    <div>
      <div
        className="hidden lg:flex w-full bg-gray-darkest flex-wrap py-6 px-20 flex-col justify-between"
        style={{ minHeight: '9rem' }}
      >
        <div className="flex justify-between items-center flex-wrap">
          <div className="flex justify-center xsm:mb-8">
            <i className="fab fa-facebook-f text-white mr-8 text-2xl" />
            <i className="fab fa-linkedin text-white mr-8 text-2xl" />
            <i className="fab fa-linkedin-in text-white text-2xl" />
          </div>
          <div className="flex justify-center xsm:mb-8 items-center">
            <ul className="flex m-0 p-0 text-white">
              <li className="pr-10 text-base uppercase">
                Terms and Condition <span className="ml-9">|</span>
              </li>
              <li className="pr-10 text-base uppercase">
                Privacy Policy <span className="ml-9">|</span>
              </li>
              <li className="pr-10 text-base uppercase">Cookie Policy</li>
            </ul>
            <div className="flex text-white items-center justify-center ml-12 shadow-md py-3 px-3">
              <i className="fas fa-headset text-2xl pr-3" />
              <div>
                <p className="uppercase text-xs font-medium mb-0">Need help?</p>
                <p className="uppercase text-base font-medium">
                  Please contact support
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-center w-full">
          <p className="text-white uppercase text-xs font-medium">
            VIRTUAL CORPORATE ACCESS <span className="mx-5">|</span>
          </p>
          <p className="text-white uppercase text-xs font-medium mr-5">
            POWERED BY STORYBOARD COMMUNICATION CORPERATION
          </p>
          <p className="text-white uppercase text-xs font-medium">
            (c) 2021 ALL RIGHTS RESERVED{' '}
          </p>
        </div>
      </div>
      <div
        className="lg:hidden hidden md:flex w-full bg-gray-darkest flex-wrap py-6 px-20 flex-col justify-between"
        style={{ minHeight: '9rem' }}
      >
        <div className="flex justify-between items-center flex-wrap">
          <div className="flex justify-center xsm:mb-8">
            <i className="fab fa-facebook-f text-white mr-8 text-2xl" />
            <i className="fab fa-linkedin text-white mr-8 text-2xl" />
            <i className="fab fa-linkedin-in text-white text-2xl" />
          </div>
          <ul className="flex m-0 p-0 text-white">
            <li className="pr-10 text-xs uppercase">
              Terms and Condition <span className="ml-3">|</span>
            </li>
            <li className="pr-10 text-xs uppercase">
              Privacy Policy <span className="ml-3">|</span>
            </li>
            <li className="text-xs uppercase">Cookie Policy</li>
          </ul>
          <div className="flex text-white items-center justify-center shadow-md py-3 px-5">
            <i className="fas fa-headset text-base pr-3" />
            <div>
              <p className="uppercase text-xs font-medium mb-0">Support?</p>
            </div>
          </div>
        </div>
        <div className="flex justify-between w-full">
          <p className="text-white uppercase text-xs font-medium">
            VIRTUAL CORPORATE ACCESS
          </p>
          <p className="text-white uppercase text-xs font-medium mr-5">
            POWERED BY STORYBOARD COMMUNICATION CORPERATION
          </p>
          <p className="text-white uppercase text-xs font-medium">
            (c) 2021 ALL RIGHTS RESERVED{' '}
          </p>
        </div>
      </div>
      <div className="sm:flex lg:hidden md:hidden w-full h-auto bg-gray-darkest py-8 pl-3">
        <ul className="flex mb-8 p-0 text-white justify-around w-full">
          <li className="text-base uppercase" style={{ fontSize: 10 }}>
            Terms and Condition
          </li>
          <li className="text-base uppercase" style={{ fontSize: 10 }}>
            Privacy Policy
          </li>
          <li className="text-base uppercase" style={{ fontSize: 10 }}>
            Cookie Policy
          </li>
        </ul>
        <div className="flex justify-around mb-8 w-2/3 mx-auto">
          <i className="fab fa-facebook-f text-white text-base" />
          <i className="fab fa-linkedin text-white text-base" />
          <i className="fab fa-linkedin-in text-white text-base" />
        </div>
        <div className="flex text-white items-center justify-center mb-8 shadow-md w-1/2 mx-auto py-3">
          <i className="fas fa-headset text-base pr-3" />
          <div>
            <p className="uppercase text-xs font-medium mb-0">Support?</p>
          </div>
        </div>
        <p
          className="text-white uppercase text-xs font-medium text-center mb-8"
          style={{ fontSize: 10 }}
        >
          VIRTUAL CORPORATE ACCESS
        </p>
        <p
          className="text-white uppercase text-xs font-medium text-center mb-8"
          style={{ fontSize: 10 }}
        >
          POWERED BY STORYBOARD COMMUNICATION CORPERATION
        </p>
        <p
          className="text-white uppercase text-xs font-medium text-center"
          style={{ fontSize: 10 }}
        >
          (c) 2021 ALL RIGHTS RESERVED{' '}
        </p>
      </div>
    </div>
  );
}
