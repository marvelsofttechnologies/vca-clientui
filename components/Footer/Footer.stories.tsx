import { withKnobs } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import React from 'react';

import { Footer } from './Footer';

storiesOf('Footer', module)
  .addDecorator(withKnobs)
  .add('Footer Demo', () => <Footer />);
