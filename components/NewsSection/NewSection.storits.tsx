import { withKnobs } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import React from 'react';

import data from '../../data.json';
import { News } from '../NewsCard/NewsCard';
import { NewsSection } from './NewsSection';
const ParsedData = JSON.parse(JSON.stringify(data));

const allNews: News[] = ParsedData.news;

storiesOf('NewsSection', module)
  .addDecorator(withKnobs)
  .add('NewsSection Demo', () => <NewsSection news={allNews} />);
