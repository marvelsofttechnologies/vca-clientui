import React from 'react';

import { News, NewsCard } from '../NewsCard/NewsCard';
import { Pagination } from '../Pagination/Pagination';

export function NewsSection({ news }: { news: News[] }) {
  return (
    <>
      <div className="container mx-auto p-10 pt-16 font-sans">
        <div className="grid lg:grid-cols-4 gap-4 w-full  md:grid-cols-3 sm:grid-cols-2 grid-cols-1">
          {news.map((record: News, index: number) => (
            <NewsCard key={index} news={record} />
          ))}
        </div>
        <Pagination />
      </div>
    </>
  );
}
