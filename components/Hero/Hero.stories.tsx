import { withKnobs } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import React from 'react';

import data from '../../data.json';
const ParsedData = JSON.parse(JSON.stringify(data));
Hero;
const hero = ParsedData.hero;

import { Hero } from './Hero';

storiesOf('Hero', module)
  .addDecorator(withKnobs)
  .add('Hero Demo', () => <Hero hero={hero} />);
