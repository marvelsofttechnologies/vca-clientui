import { withKnobs } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import React from 'react';

import { Pagination } from './Pagination';

storiesOf('Pagination', module)
  .addDecorator(withKnobs)
  .add('Pagination Demo', () => <Pagination />);
