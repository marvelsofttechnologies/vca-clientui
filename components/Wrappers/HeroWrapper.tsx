import { useQuery } from '@apollo/client';
import { StyledSpinnerNext } from 'baseui/spinner';
import React from 'react';

import { PAGE_QUERY } from '../../queries/queries';
import { Hero, HeroModel } from '../Hero/Hero';

export function HeroWrapper() {
  let page = {
    hero: {
      mediaUrl: '',
      mainText: '',
      subText: '',
      heading: '',
    },
  };
  const { loading: pageLoading, error: pageError, data: pageData } = useQuery(
    PAGE_QUERY
  );
  if (!pageLoading && !pageError) {
    page = pageData.page;
  }
  const hero: HeroModel = page.hero;
  return (
    <>
      {pageLoading ? (
        <div className="flex justify-center align-middle h-screen">
          <div className="justify-center align-middle">
            <StyledSpinnerNext />
          </div>
        </div>
      ) : (
        <Hero hero={hero} />
      )}
    </>
  );
}
