import { useQuery } from '@apollo/client';
import { StyledSpinnerNext } from 'baseui/spinner';
import React from 'react';

import { SITE_QUERY } from '../../queries/queries';
import { Header } from '../Header/Header';

export function HeaderWrapper() {
  let site = {
    header: {
      name: '',
      type: '',
      logoUrl: '',
      menuItems: [],
    },
  };
  const { loading, error, data } = useQuery(SITE_QUERY);
  if (!loading && !error) {
    site = data.site;
  }
  const header = site.header;

  return (
    <>
      {loading ? (
        <div className="flex justify-center align-middle h-screen">
          <div className="justify-center align-middle">
            <StyledSpinnerNext />
          </div>
        </div>
      ) : (
        <Header header={header} />
      )}
    </>
  );
}
