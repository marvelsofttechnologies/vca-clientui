import Link from 'next/link';
import * as React from 'react';

export interface Header {
  name: string;
  type: string;
  logoUrl: string;
  menuItems: MenuItem[];
}

export interface MenuItem {
  name: string;
  slug: string;
  description: string;
}

const MenuItems = ({ menuItems }: { menuItems: MenuItem[] }) => (
  <>
    {menuItems.map((menuItem, index) => (
      <Link href={menuItem.slug} key={index}>
        <a
          href={menuItem.slug}
          className="border-transparent font-normal text-primary hover:border-gray-300 hover:text-gray-700 inline-flex items-center px-1 pt-1 border-b-2 text-lg "
        >
          {menuItem.name}
        </a>
      </Link>
    ))}
  </>
);

export function Header({ header }: { header: Header }): JSX.Element {
  const [mobileMenuVisibility, setMobileHeaderVisibility] = React.useState(
    false
  );
  return (
    <nav className="bg-navbg shadow font-sans">
      <div className="max-w-7xl mx-auto px-2 sm:px-4 lg:px-8">
        <div className="flex justify-between h-16">
          <div className="flex px-2 lg:px-0 justify-between">
            <div className="flex-shrink-0 flex items-center">
              <img
                className="block lg:hidden h-8 w-auto"
                src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
                alt="Workflow"
              />
              <img
                className="hidden lg:block h-8 w-auto"
                src="https://tailwindui.com/img/logos/workflow-logo-indigo-600-mark-gray-800-text.svg"
                alt="Workflow"
              />
            </div>
          </div>
          <div className="hidden lg:ml-6 lg:flex lg:space-x-8">
            <MenuItems menuItems={header.menuItems} />
            <div className="flex-1 flex items-center justify-center px-2 lg:ml-6 lg:justify-end">
              <div className="max-w-lg w-full lg:max-w-xs">
                <label className="sr-only">Search</label>
                <div className="relative">
                  <div className="absolute inset-y-0 left-0  flex items-center pointer-events-none">
                    <svg
                      className="h-8 w-8 text-primary"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                      aria-hidden="true"
                    >
                      <path
                        fillRule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                  {/* <input
                  id="search"
                  name="search"
                  className="block w-full pl-10 pr-3 py-2 border border-gray-300 rounded-md leading-5 bg-white placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  placeholder="Search"
                  type="search"
                /> */}
                </div>
              </div>
            </div>
          </div>
          <div className="flex items-center lg:hidden">
            <button
              type="button"
              className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500"
              aria-controls="mobile-menu"
              aria-expanded="false"
              onClick={() => setMobileHeaderVisibility(!mobileMenuVisibility)}
            >
              <span className="sr-only">Open main menu</span>
              <svg
                className={
                  mobileMenuVisibility ? 'block h-6 w-6' : 'hidden h-6 w-6'
                }
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16M4 18h16"
                />
              </svg>
              <svg
                className={
                  mobileMenuVisibility ? 'hidden h-6 w-6' : 'block h-6 w-6'
                }
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
      <div className="lg:hidden w-full" id="mobile-menu">
        <div
          className="pt-2 pb-3 space-y-1 w-full justify-center bg-white fixed h-full z-50 "
          style={{ display: mobileMenuVisibility ? 'none' : 'block' }}
        >
          {header.menuItems.map((menuIten: MenuItem, index: number) => (
            <a
              key={index}
              href={menuIten.slug}
              className="border-transparent text-gray-600 flex justify-center hover:bg-gray-50 hover:border-gray-300 hover:text-gray-800 block pl-3 pr-4 py-2 border-l-4 text-base font-medium "
            >
              {menuIten.name}
            </a>
          ))}
        </div>
      </div>
    </nav>
  );
}
