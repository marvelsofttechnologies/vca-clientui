import React from 'react';

import { render } from '../../test/testUtils';
import { Header } from './Header';

test('matches snapshot', () => {
  const header = {
    name: 'Header name',
    type: 'Type 1',
    logoUrl: 'https://apple.ca',
    menuItems: [
      {
        name: 'HOME',
        slug: '/home',
        description: 'Home menu item',
      },
      {
        name: 'ABOUT US',
        slug: '/about_us',
        description: 'ABOUT US menu item',
      },
      {
        name: 'TEAM',
        slug: '/team',
        description: 'TEAM menu item',
      },
    ],
  };
  const { asFragment } = render(<Header header={header} />);
  expect(asFragment()).toMatchSnapshot();
});
