import { withKnobs } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import React from 'react';

import data from '../../data.json';
const ParsedData = JSON.parse(JSON.stringify(data));

import { Header } from './Header';
const header = ParsedData.header;

storiesOf('Header', module)
  .addDecorator(withKnobs)
  .add('Header Demo', () => <Header header={header} />);
