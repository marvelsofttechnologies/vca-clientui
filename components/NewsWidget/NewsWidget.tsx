import React, { useRef } from 'react';
import Slider from 'react-slick';

import { News, NewsCard } from '../NewsCard/NewsCard';
import { boolean } from '@storybook/addon-knobs';

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 4,
  initialSlide: 4,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

export function NewsWidget({ news,contain= true }: { news: News[],contain:boolean}) {
  const sliderRef = useRef();

  const gotoNext = () => {
    if (sliderRef != null) {
      sliderRef.current.slickNext();
    }
  };

  const gotoPrevious = () => {
    sliderRef.current.slickPrev();
  };
  return (
    contain ? 
    <div className="container mx-auto  p-10 pt-16 font-sans">
      <div className="mb-10 ">
        <h1 className="uppercase font-bold text-3xl">Latest News</h1>
        <h1 className="text-primary text-xl font-bold">
          Catch all the information on time here
        </h1>
      </div>
      <div className="flex mb-2 justify-end">
        <div
          className="bg-primary text-white py-1 px-4 mr-1"
          onClick={gotoPrevious}
        >
          <i className="fas fa-chevron-left"></i>
        </div>
        <div className="bg-primary text-white py-1 px-4" onClick={gotoNext}>
          <i className="fas fa-chevron-right"></i>
        </div>
      </div>
      <Slider className="" {...settings} ref={sliderRef}>
        {news.map((record: News, index: number) => (
          <NewsCard key={index} news={record} />
        ))}
      </Slider>
    </div>
  : 
  <>
  <div className="flex mb-2 justify-end">
        <div
          className="bg-primary text-white py-1 px-4 mr-1"
          onClick={gotoPrevious}
        >
          <i className="fas fa-chevron-left"></i>
        </div>
        <div className="bg-primary text-white py-1 px-4" onClick={gotoNext}>
          <i className="fas fa-chevron-right"></i>
        </div>
      </div>
      <Slider className="" {...settings} ref={sliderRef}>
        {news.map((record: News, index: number) => (
          <NewsCard key={index} news={record} />
        ))}
      </Slider>
  </>
 );
}
