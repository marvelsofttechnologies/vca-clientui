import React from 'react';

export const SortViewOption = () => {
    return (
        <>
        <div className="lg:flex md:flex justify-between mt-10 sm:block">
            <div className="lg:flex md:flex justify-between sm:block">
                <button className="bg-red-800 mr-5 w-28 h-11 text-white">By Month</button>
                <button className="bg-red-800 mr-5 w-28 h-11 text-white">By Month</button>
                <button className="bg-red-800 mr-5 w-28 h-11 text-white">By Month</button>
            
            </div>
            <div className="lg:flex md:flex justify-between sm:block">
                <p className="text-xl font-bold">View :</p>
                <div className="text-red-800 text-4xl ml-5"><i className="fas fa-list"></i></div>
                <div className="text-red-800 text-4xl ml-5"><i className="fas fa-th"></i></div>
            </div>
        </div>
        </>
    )
}