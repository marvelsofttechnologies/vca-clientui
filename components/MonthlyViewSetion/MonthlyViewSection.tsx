import React from 'react';

import jsonData from '../../data.json';
import { NewsWidget } from '../NewsWidget/NewsWidget';

export const MonthlyViewSection = () => {
  const ParsedData = JSON.parse(JSON.stringify(jsonData));
  const allNews = ParsedData.news;
  return (
    <>
      <div className="container mx-auto p-10 font-roboto">
        <h2 className="uppercase font-light">This Month</h2>
        <NewsWidget  news={allNews} contain={false}/>
        <hr />
      </div>
    </>
  );
};
