import React from 'react';
import { SortViewOption } from '../SortViewOptions/SortViewOption';

export const NewsHero = () => {
  return (
    <>
      <div className="container mx-auto  p-10 font-roboto">
          <ul className="uppercase flex mb-5">
              <li className="pr-5 text-sm text-red-400">Latest</li>
              <li className="pr-5 text-sm text-red-400">Popular</li>
              <li className="pr-5 text-sm text-red-400">Category</li>
          </ul>
        <div className="mb-5 ">
          <h1 className="uppercase font-bold text-2xl font-roboto">Latest News</h1>
          <h1 className="textlg ">
            See what we have been up to
          </h1>
        </div>
        <div className="relative w-full container mx-auto h-64 sm:h-72 md:h-96 lg:h-96">
        <img
              className=" w-full object-cover"
              src={'https://res.cloudinary.com/harmonic-coder/image/upload/v1615941788/starting-new-business-e1530648542386_ubgtcf.png'}
              alt="hero image"
            />
        </div>
        <div className="mt-24">
          <h1 className="uppercase font-bold text-2xl">Our stock rose by 350%</h1>
          <h1 className="textlg ">
            See what we have been up to
          </h1>
        </div>
      <SortViewOption/>
      </div>
    </>
  );
};
