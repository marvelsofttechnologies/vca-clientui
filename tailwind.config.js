//const colors = require('tailwindcss/colors');
module.exports = {
  purge: ['./pages/**/*.tsx', './components/**/*.tsx'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    sans: ['roboto'],
    extend: {
      height: {
        hero: '551px',
      },
      colors: {
        gray: {
          darkest: '#424242',
          dark: '#808080',
          DEFAULT: '#c4c4c4',
          light: '#c4c4c4',
          lightest: '#eaeaea',
        },
        black: {
          light: '#212121',
        },
        white: '#ffffff',
        warning: '#E8b601',
        danger: '#c9585c',
        primary: '#033E96',
        navbg: 'F2F2F2',
      },
      fontFamily: {
        roboto: 'Roboto',
      },
      screens: {
        xsm: { max: '630px' },
        sm: '640px',
        md: '768px',
        lg: '1024px',
        xl: '1280px',
        '2xl': '1536px',
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
  ],
};
